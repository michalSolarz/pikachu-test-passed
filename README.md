Usage:
===
Run `pikachu-test-passed [smaller | s | smallerColoured | sc]` after your test passed, and you will surprised Pikachu.

Happy testing :)

ASCII images were extracted from this Reddit thread: https://www.reddit.com/r/ProgrammerHumor/comments/a381ur/the_correct_reaction_to_unit_tests_passing/