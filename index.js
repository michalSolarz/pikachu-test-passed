#!/usr/bin/env node

const smaller = require('./images').smaller;
const smallerColoured = require('./images').smallerColoured;
const standard = require('./images').standard;

const resolveImage = (arg) => {
    if (arg === undefined || arg === null) {
        return standard;
    }
    arg = arg.toLowerCase();
    switch (arg) {
        case 'smaller':
        case 's':
            return smaller;
        case 'smaller_coloured':
        case 'smallercoloured':
        case 'sc':
            return smallerColoured;
        default:
            return standard;
    }
};

const image = resolveImage(process.argv.slice(2)[0]);

console.log(image);
